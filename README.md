### This repo reviews general computational project organization with a focus on using a markdown file as an electronic lab notebook, and using git and bitbucket for version control.  


<u>You will need:</u>  

* a bitbucket account  
* a text editor (eg Sublime Text (recommended), Textwrangler, etc)

#### See details on structuring and using your electronic lab notebook in the link to notebook.md above

Optional tools:   

-You can add a MarkdownEditing plugin to Sublime Text so the Markdown text file is formatted nicely when taking notes in Sublime Text (see installation details below).  

Notebook.md gives instructions for starting the project.
I use the same general formatting in Notebook.md for each new project I begin.
Date each entry and list what you did that day (which scripts you ran, what bash commands, etc.)

Some useful links for Markdown formatting:  

https://guides.github.com/features/mastering-markdown/  
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet  

Installing Markdown Editing plugin for Sublime Text

-First Install Sublime Package Control:

-Open Sublime and open the command palette (Win/Linux: ctrl+shift+p, Mac: cmd+shift+p)

-Type Install Package Control, press enter

-Next install the MarkdownEditing package

-Navigate to Sublime Text Menu > Preferences > Package Control > Package Control: Install Package

-Type Markdownediting, select that package and hit enter

-Select and customize your color schemes with Preferences > Select Color Scheme...

