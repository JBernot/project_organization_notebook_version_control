8/29/19 JPB
--------

- Go to bitbucket and create a new repo
- Click the Clone button on the top right and copy the git clone text
- Go to the directory you want to store your repository in (e.g., repo/<directory>)
- clone the bitbucket repo onto your computer. For this project, that lookeded like:
```bash
git clone git@bitbucket.org:JBernot/project_organization_notebook_version_control.git
```
- create notebook.md and README.md using a text editor and save them in repo/<directory>
- add notebook.md and README.md to git on your computer:
```bash
git add notebook.md
git add README.md
```
- commit your changes to git
```bash
git commit -am "adding notebook.md and README.md"
```
- push your changes to the git repo
```bash
git push -u origin master

```
- Check your bitbucket repository to see if if your changes have been implemented. You can also view files here and review past commits.

Project layout
--------

Typically I lay my projects out like this:
/repo/<project_name>  

<project_name> will be the repository  

Within <project_name> will be, at a minimum, subdirectories for data/ and scripts/
  You may also want to include subdirectories for analysis and results  


I typically use this workflow when starting a new project:
- create the bitbucket project
- clone the bitbucket project to my local machine and add the notebook, readme, and scripts subdirectories (then push to bitbucket)
- then cone the bitbucket repository onto the HPC I am working on  in a Projects/ directory  
  eg /groups/cbi/Projects/barnacles and/or
  /lustre/groups/cbi/Projects/barnacles
- create a data subdir on the HPC

Backing up a project
--------

You should regularly backup and projects from active run partitions (e.g., /lustre) to longer term storage (e.g., /groups or /import).


rsync is the best tool for this.

You can find details for rsync on most Linux systems by viewing the manual page with
```bash
man rsync
```

My typical rsync commands look like this:
```bash
rsync -avh /lustre/groups/cbi/Projects/copepod_ortholog_iding /groups/cbi/Projects --log-file=/lustre/groups/cbi/Projects/copepod_ortholog_iding/rsync_5_7_19.log
# commands for rsync are rsync <source> <destination>
```

#### NOTE: be aware of trailing "/" in rsync: rsync source destination will copy the source directory and all subdirs to the destination, while rsync source/ destination will ommit the source dir and will copy all of the subdirs to the destination

explanation of options:
-a is archive mode, equals -rlptgoD (typically you will want this mode)
   -r = recursive  
   -l = include symlinks  
   -p = preserve permissions  
   -t = preserve modification times  
   -og = preserves owner group rights  
   -D = preserves device and special files (dont worry about this)  
-v = verbose mode  
-h = shows sizes in human readable format eg MB/GB/TB in stead of bytes, and shows progress during transer  
--logile-file =/path/to/filename saves a copy of the log, including success or any errors, to a location of your choice (I usually use the source destination so I can see the last time I did a backup)  

Like everything else you do on the command line, you should include notes about your rsync backsups in your notebook.md

tmux is also useful to keep rsync running in the background on HPC even if you disconnect. Tmux is included in many Linux installations. To use tmux with rsync you would type:
```bash
tmux
rsync <source> <destination>
# then press CTRL + b then d (hold ctrl, press b, let go of both of the keys, and press d) to disconnect from tmux
# this will allow rsync to proceed in the background
```

Other tipcs and tricks:
aliases
--------

These are shell-specific keyboard shortcuts you can save in your .bash_rc on your computer and/or on the HPC. 
They can make your life easier.  
Here are some that I commonly use:
```bash
alias br='. ~/.bash_profile'
alias lt='ls -lht'
alias eb='sublime ~/.bash_profile'
alias gc='git commit -am '
alias hg='history | grep'

alias colone='ssh jbernot@login.colonialone.gwu.edu'
alias colone3='ssh jbernot@login3.colonialone.gwu.edu'
alias colone4='ssh jbernot@login4.colonialone.gwu.edu'
alias colone5='ssh jbernot@login5.colonialone.gwu.edu'
alias coltwo='ssh jbernot@pegasus.colonialone.gwu.edu'
alias coltwo1='ssh jbernot@log001.colonialone.gwu.edu'
alias coltwo2='ssh jbernot@log002.colonialone.gwu.edu'
alias coltwo3='ssh jbernot@log003.colonialone.gwu.edu'
alias coltwo4='ssh jbernot@log004.colonialone.gwu.edu'

alias scpcolone='echo What is the absolute path to the file/s to be copied to HPC?
read scppath_colone
echo scp -r $scppath_colone jbernot@login.colonialone.gwu.edu:~ | bash -'
alias scplocal='echo What is the absolute path to the file/s to be copied to my local machine?
read scppath_local
echo scp -r jbernot@login.colonialone.gwu.edu:$scppath_local . | bash -'
alias today='grep -h $(date +"%m/%d") /usr/share/calendar/calendar.*'
```
